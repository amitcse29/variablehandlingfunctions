<?php
$emptyFunctionCheckVariable = 0;

// Evaluates to true because $emptyFunctionCheckVariable is empty
if (empty($emptyFunctionCheckVariable)) {
    echo '$emptyFunctionCheckVariable is either 0, empty, or not set at all';
}