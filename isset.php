
<?php

$nullVariable = '';

// This will evaluate to TRUE so the text will be printed.
if (isset($nullVariable)) {
    echo "This var is set so I will print.";
}

// In the next examples we'll use var_dump to output
// the return value of isset().

$testOne = "test";
$testAnother = "anothertest";

var_dump(isset($testOne));      // TRUE
var_dump(isset($testOne, $testAnother)); // TRUE

unset ($testOne);

var_dump(isset($testOne));     // FALSE
var_dump(isset($testOne, $testAnother)); // FALSE

$nullExample = NULL;
var_dump(isset($nullExample));   // FALSE


