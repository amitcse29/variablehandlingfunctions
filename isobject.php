
<?php
// Declare a simple function to return an 
// array from our object
function get_students($object)
{
    if (!is_object($object)) {
        return false;
    }

    return $object->students;
}

// Declare a new class instance and fill up 
// some values
$object = new stdClass();
$object->students = array('Kalle', 'Ross', 'Felipe');

var_dump(get_students(null));
var_dump(get_students($object));
